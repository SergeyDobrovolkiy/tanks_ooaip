#include <queue>
#include "../src/hs/ICommand.h"
#include "../src/hs/IGameQueue.h"

class GameQ: public IGameQueue
{
private:
    std::queue<ICommand*> m_q;
public:
    GameQ() = default;

    void push(ICommand* command) override
    {
        m_q.push(command);
    }

    void pop() override 
    {
        m_q.pop();
    }

    ICommand* front() override
    {
        return m_q.front();
    }

    int size() 
    {
        return m_q.size();
    }

    ~GameQ() = default;
};