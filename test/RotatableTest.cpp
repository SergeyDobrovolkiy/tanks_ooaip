#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "../src/hs/UObject.h"

#include "../src/Rotatable/CommandRotate.h"
#include "../src/Rotatable/CommandStartRotate.h"
#include "../src/Rotatable/CommandEndRotate.h"

#include "./Mocks/RotatableMock.h"
#include "GameQ.h"

#include <iostream>

std::vector<int> operator+(const std::vector<int> &firstVector, const std::vector<int> &secondVector)
{
    std::vector<int> result;
    if (firstVector.size() > secondVector.size())
    {
        for (int i = 0; i < secondVector.size(); i++)
        {
            result.push_back(firstVector[i] + secondVector[i]);
        }
        return result;
    }
    else if (firstVector.size() < secondVector.size())
    {
        for (int i = 0; i < firstVector.size(); i++)
        {
            result.push_back(firstVector[i] + secondVector[i]);
        }
        return result;
    }
    else
    {
        for (int i = 0; i < secondVector.size(); i++)
        {
            result.push_back(firstVector[i] + secondVector[i]);
        }
        return result;
    }
}

bool vectorEachValEqual(const std::vector<int>& vec, int val)
{
    for (const int& el : vec)
    {
        if (el != val)
        {
            return false;
        }
    }

    return true;
}

TEST(Rotatable, Mocks)
{
	RotatableMock<int> mcrt;

	EXPECT_CALL(mcrt, getAngle())
		.Times(1)
		.WillRepeatedly(testing::Return(std::vector<int> { 5, 8 }));
	auto res = mcrt.getAngle();
	EXPECT_TRUE(res.front() == 5);
	EXPECT_TRUE(res[res.size() - 1] == 8);
}

TEST(Rotatable, CommandRotate)
{
	std::vector<int> angle = {60 };
	const std::vector<int> velocity = { 30 };

	RotatableMock<int> mcrt;

	EXPECT_CALL(mcrt, setAngle).Times(2);
	EXPECT_CALL(mcrt, setAngleVelocity).Times(1);

	EXPECT_CALL(mcrt, getAngle).Times(2);
	EXPECT_CALL(mcrt, getAngleVelocity).Times(1);

	mcrt.setAngle(angle);
	mcrt.setAngleVelocity(velocity);

	CommandRotate<int> commandRotate(mcrt);
	commandRotate.exec();

	EXPECT_EQ(mcrt.getAngle(),(angle + velocity)%360);
}

TEST(Rotatable, CommandRotateMoreThan360)
{
	std::vector<int> angle = { 350 };
	const std::vector<int> velocity = { 30 };

	RotatableMock<int> mcrt;

	EXPECT_CALL(mcrt, setAngle).Times(2);
	EXPECT_CALL(mcrt, setAngleVelocity).Times(1);

	EXPECT_CALL(mcrt, getAngle).Times(2);
	EXPECT_CALL(mcrt, getAngleVelocity).Times(1);

	mcrt.setAngle(angle);
	mcrt.setAngleVelocity(velocity);

	CommandRotate<int> commandRotate(mcrt);
	commandRotate.exec();

	EXPECT_EQ(mcrt.getAngle(), (angle + velocity) % 360);
}

TEST(Rotatable, CommandRotateQueue)
{
	UObject obj;
	std::vector<int> angle = { 120 };
	std::vector<int> velocity = { 30 };

	GameQ gameQueue;

	RotatableMock<int> mcrt;

	EXPECT_CALL(mcrt, setAngle).Times(3);
	EXPECT_CALL(mcrt, setAngleVelocity).Times(4);

	EXPECT_CALL(mcrt, getAngle).Times(5);
	EXPECT_CALL(mcrt, getAngleVelocity).Times(3);

	mcrt.setAngle(angle);

	CommandStartRotate<int> commandStart(obj, gameQueue, mcrt, velocity);
	gameQueue.push(static_cast<ICommand*>(&commandStart));

	CommandEndRotate commandEnd(obj);
	gameQueue.push(static_cast<ICommand*>(&commandEnd));


	gameQueue.front()->exec(); //Старт
	gameQueue.pop();
	EXPECT_TRUE(vectorEachValEqual(mcrt.getAngleVelocity(), 0));
	EXPECT_EQ(mcrt.getAngle(), angle + velocity);

	gameQueue.front()->exec(); //Конец
	gameQueue.pop();


	const std::vector<int> expAngle = mcrt.getAngle() + velocity; 

	gameQueue.front()->exec(); // Старт
	gameQueue.pop();

	EXPECT_EQ(mcrt.getAngle(), expAngle);

	gameQueue.front()->exec(); // Пустая
	gameQueue.pop();

	EXPECT_EQ(gameQueue.size(), 0);
}

TEST(Rotatable, CommandRotateQueueBigDegrees)
{
	UObject obj;
	std::vector<int> angle = { 50 };
	std::vector<int> velocity = { 800 };

	GameQ gameQueue;

	RotatableMock<int> mcrt;

	EXPECT_CALL(mcrt, setAngle).Times(2);
	EXPECT_CALL(mcrt, setAngleVelocity).Times(2);

	EXPECT_CALL(mcrt, getAngle).Times(2);
	EXPECT_CALL(mcrt, getAngleVelocity).Times(1);

	mcrt.setAngle(angle);

	CommandStartRotate<int> commandStart(obj, gameQueue, mcrt, velocity);
	gameQueue.push(static_cast<ICommand*>(&commandStart));

	CommandEndRotate commandEnd(obj);
	gameQueue.push(static_cast<ICommand*>(&commandEnd));

	
	angle.front() = 130; // (800+50)%360 = 130
	gameQueue.front()->exec(); // Старт
	gameQueue.pop();
	EXPECT_EQ(mcrt.getAngle().front(), angle.front());

	gameQueue.front()->exec(); // Конец
	gameQueue.pop();
}
