#include <gmock/gmock.h>
#include "../../src/Rotatable/IRotatable.h"
#include <vector>

template <typename T>
class RotatableMock: public IRotatable<T>
{
private:
    std::vector<T> m_angle;
    std::vector<T> m_angleVelocity;
public:
    MOCK_METHOD(std::vector<T>, getAngle, (), (override));
    MOCK_METHOD(std::vector<T>, getAngleVelocity, (), (override));
    MOCK_METHOD(void, setAngle, (std::vector<T> value), (override));
    MOCK_METHOD(void, setAngleVelocity, (std::vector<T> value), (override));

    RotatableMock()
    {
        ON_CALL(*this, getAngle).WillByDefault(
        [this]()
        {
            return this->m_angle;
        });

        ON_CALL(*this, getAngleVelocity).WillByDefault(
        [this]()
        {
            return this->m_angleVelocity;
        });

        ON_CALL(*this, setAngle).WillByDefault(
        [this](std::vector<T> value)
        {
            this->m_angle = value;
        });

        ON_CALL(*this, setAngleVelocity).WillByDefault(
        [this](std::vector<T> value)
        {
            this->m_angleVelocity = value;
        });
    }
};