#pragma once

class ICommand
{
public:
    virtual void exec() = 0;
    virtual ~ICommand() = default;
};
