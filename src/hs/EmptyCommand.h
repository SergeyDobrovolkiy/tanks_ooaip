#pragma once

#include "ICommand.h"

class EmptyCommand: public ICommand 
{
    void exec();
};
