#pragma once

#include "IUObject.h"
#include <map>

class UObject: public IUObject 
{
private:
    std::map<std::string, std::any> storage;

public:
    UObject() = default;

    virtual std::any getProperty(const std::string &key);
    virtual void setProperty(const std::string &key, const std::any &value);

    ~UObject() = default;
};
