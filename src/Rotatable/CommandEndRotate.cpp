#include "CommandEndRotate.h"

CommandEndRotate::CommandEndRotate(IUObject& object): m_object(object)
{
}

void CommandEndRotate::exec()
{
    m_object.setProperty("commandRotate", static_cast<ICommand*>(&m_emptyCommand));
}
