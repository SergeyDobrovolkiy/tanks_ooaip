#pragma once

#include <vector>

template <typename T>
class IRotatable
{
public:
    virtual void setAngle(std::vector<T> value) = 0;
    virtual void setAngleVelocity(std::vector<T> value) = 0;
    virtual std::vector<T> getAngle() = 0;
    virtual std::vector<T> getAngleVelocity() = 0;
    virtual ~IRotatable() = default;
};
