#pragma once

#include "../hs/IUObject.h"
#include "../hs/ICommand.h"
#include "../hs/IGameQueue.h"
#include "IRotatable.h"
#include "CommandRotate.h"

#include <vector>

template <typename T>
class CommandStartRotate: public ICommand
{
private:
    IUObject& m_object;
    IGameQueue& m_queue;
    IRotatable<T>& m_field;
    std::vector<T>& m_velocity;
    std::vector<T> m_null;
public:
    CommandStartRotate(IUObject& object, IGameQueue& queue, IRotatable<T>& field, std::vector<T>& velocity):
    m_object(object), m_queue(queue), m_field(field), m_velocity(velocity)
    {
        /* m_object = object;
        m_queue = queue;
        m_field = rotate;
        m_velocity = velocity; */
        m_null = std::vector<T>(velocity.size(), 0);
        ICommand* commandRotate = this;
        m_object.setProperty("commandRotate", commandRotate);
    }

    void exec()
    {
        m_field.setAngleVelocity(m_velocity);
        CommandRotate<T> rotate(m_field);
        rotate.exec();
        m_field.setAngleVelocity(m_null);
        m_queue.push(std::any_cast<ICommand*>(m_object.getProperty("commandRotate")));
    }
};
