#pragma once

#include "IRotatable.h"
#include "../hs/ICommand.h"

template <typename T>
std::vector<T> operator+(const std::vector<T> &firstVector, const std::vector<T> &secondVector)
{
    std::vector<T> result;
    if (firstVector.size() > secondVector.size())
    {
        for (int i = 0; i < secondVector.size(); i++)
        {
            result.push_back(firstVector[i] + secondVector[i]);
        }
        return result;
    }
    else if (firstVector.size() < secondVector.size())
    {
        for (int i = 0; i < firstVector.size(); i++)
        {
            result.push_back(firstVector[i] + secondVector[i]);
        }
        return result;
    }
    else
    {
        for (int i = 0; i < secondVector.size(); i++)
        {
            result.push_back(firstVector[i] + secondVector[i]);
        }
        return result;
    }
}

template <typename T>
std::vector<T> operator%(const std::vector<T> &Vector, int arg)
{
    std::vector<T> result;

    for (int i = 0; i < Vector.size(); i++)
    {
        result.push_back(Vector[i] % arg);
    }
    return result;
}

template <typename T>
class CommandRotate: public ICommand
{
private:
    IRotatable<T> &m_field;
public:
    CommandRotate(IRotatable<T> &field): m_field(field)
    {
    }

    void exec()
    {
        const std::vector<T> &&angle = m_field.getAngle();
        const std::vector<T> &&angleVelocity = m_field.getAngleVelocity();
        m_field.setAngle((angle + angleVelocity)%360);
    }
    ~CommandRotate() = default;
};
