#pragma once

#include "../hs/IUObject.h"
#include "../hs/ICommand.h"
#include "../hs/IGameQueue.h"
#include "IRotatable.h"
#include "CommandRotate.h"
#include "../hs/EmptyCommand.h"

#include <vector>

class CommandEndRotate: public ICommand
{
private:
    IUObject &m_object;
    EmptyCommand m_emptyCommand;
public:
    CommandEndRotate(IUObject &object);

    void exec();   
};
