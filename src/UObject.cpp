#include "hs/UObject.h"

std::any UObject::getProperty(const std::string &key) 
{
    /* if (!storage.count(key))
    {
        throw 
    } */
    return storage[key];
}

void UObject::setProperty(const std::string &key, const std::any &value)
{
    storage[key] = value;
}
